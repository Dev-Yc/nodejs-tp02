// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";


// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration

const firebaseConfig = {

  apiKey: "AIzaSyAf3V9GAu0pXgaKNvtULmsFTQB8Ubalj7M",

  authDomain: "wsf-react-84ac8.firebaseapp.com",

  projectId: "wsf-react-84ac8",

  storageBucket: "wsf-react-84ac8.appspot.com",

  messagingSenderId: "729717692202",

  appId: "1:729717692202:web:d23ebef52fe2a89b50f053"

};


// Initialize Firebase

const app = initializeApp(firebaseConfig);
// Initialize Firebase



// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);
const db = getFirestore(app);
export {app, auth, db}
