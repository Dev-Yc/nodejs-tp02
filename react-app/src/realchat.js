import React, { Component } from "react";
import './style.css';

class Chat extends Component {
render() {
  return(
<html lang="en">
  <head>
    <script src="https://kit.fontawesome.com/55ab003ead.js" crossorigin="anonymous"></script>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Messenger</title>

    <link rel="stylesheet" href="./style.css" />
    <link rel="icon" href="./favicon.svg" />
  </head>
  <body>
    <div class="full-height">
      <header><h1>Messenger</h1></header>
      <main>
        <div class="top-space"></div>
      </main>
    </div>
    <form id="new-message">
      <div id="pseudoMessage">
        <div>
          <label for="pseudo">Pseudo</label>
          <input
            id="pseudo"
            name="pseudo"
            placeholder="Exemple : John Cenaaaa"
            autocomplete="off"
          />
        </div>
        <div>
          <label for="body">Message</label>
          <input
            id="body"
            name="body"
            placeholder="Entrer un message..."
            autocomplete="false"
          />
        </div>
      </div>
      <div class="buttonSubmit">
        <button type="submit" class="send">
          <i class='fa fa-send'></i></button>
      </div>
    </form>
    <script src="./main.js" type="module"></script>

  </body>
</html>
  )
}
}

export default Chat;