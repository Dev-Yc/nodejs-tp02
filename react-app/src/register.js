import { createUserWithEmailAndPassword } from "firebase/auth";
import { useCallback, useState } from "react";
import { auth, db } from "./firebase";
import { addDoc, collection } from "firebase/firestore";
import { useNavigate, Link } from "react-router-dom";

export default function Register() {
    const [pseudo, setPseudo] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const navigate = useNavigate();
    const handlePseudoChange = useCallback(
        (e) => {
            setPseudo(e.target.value);
        },
        [setPseudo]
    );

    const handleEmailChange = useCallback(
        (e) => {
            setEmail(e.target.value);
        },
        [setEmail]
    );

    const handlePasswordChange = useCallback(
        (e) => {
            setPassword(e.target.value);
        },
        [setPassword]
    );

    const handleSubmit = useCallback(
        (e) => {
            e.preventDefault();
            createUserWithEmailAndPassword(auth, email, password)
            .then(async(userCredential) => {
                // Signed in 
                const user = userCredential.user;
                const docRef = await addDoc(collection(db, "user"), {
                    uid: user.uid,
                    pseudo: pseudo
                  });
                  console.log("Document written with ID: ", docRef.id);
                console.log(userCredential, user);
                navigate("/login");
            })
            .catch((error) => {
                setError(error.message);
                // ..
            });
        },
        [pseudo, email, password, setError]
    )

    return(
        <div>
            <h1>Register</h1>
                <form onSubmit={handleSubmit}>
                <label>
                        Pseudo :
                        <input type="pseudo" name="pseudo" onChange={handlePseudoChange} />
                    </label>
                    <label>
                        Email :
                        <input type="email" name="email" onChange={handleEmailChange} />
                    </label>
                    <label>
                        Password :
                        <input type="password" name="password" onChange={handlePasswordChange} />
                    </label>
                    {error && <p>{error}</p>}
                    <button type="submit">Register</button>
                </form>
                <p>Déjà un compte ?</p><Link to="/login"><button>Connecte-toi</button></Link>
        </div>
    )
}