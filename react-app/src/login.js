import { signInWithEmailAndPassword } from "firebase/auth";
import { collection, getDocs, query, where } from "firebase/firestore";
import { useCallback, useState } from "react";
import { auth, db } from "./firebase";
import { useNavigate } from "react-router-dom";

export default function Login() {
    const navigate = useNavigate();
    const [user, setUser] = useState(null);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");

    const handleEmailChange = useCallback(
        (e) => {
            setEmail(e.target.value);
        },
        [setEmail]
    );

    const handlePasswordChange = useCallback(
        (e) => {
            setPassword(e.target.value);
        },
        [setPassword]
    );
    
    const handleSubmit = useCallback(
        (e) => {
            e.preventDefault();
            signInWithEmailAndPassword(auth, email, password)
            .then(async(userCredential) => {
                // Signed in 
                const user = userCredential.user;
                const q = await getDocs (query(collection(db, "user"), where("uid", "==", user.uid)));
                setUser({user,pseudo:q.docs[0].data().pseudo})
                console.log(userCredential, user, q.docs);
                navigate("/chat")
            })
            .catch((error) => {
                setError(error.message);
                // ..
            });
        },
        [setUser, email, password,  setError]
    )

    return(
        <div>
            <h1>Login</h1>
                <form onSubmit={handleSubmit}>
                    <label>
                        Email :
                        <input type="email" name="email" onChange={handleEmailChange} />
                    </label>
                    <label>
                        Password :
                        <input type="password" name="password" onChange={handlePasswordChange} />
                    </label>
                    {error && <p>{error}</p>}
                    <button type="submit">login</button>
                </form>
                {user && <p>{user.pseudo}</p>}
        </div>
    )
}